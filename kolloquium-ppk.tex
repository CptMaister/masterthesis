\documentclass[12pt,utf8,notheorems,compress,t,dvipsnames]{beamer}

\usetheme{Boadilla}

\title{Bifurcations and $J^+$-like invariants in the rotating Kepler problem}
\author{Alexander Mai}
\institute{University of Augsburg}
\date{01. Juli 2024}

\graphicspath{{img/kolloquium/}}



\usepackage[german]{babel}
\usepackage{xcolor}               % extra color names
\usepackage{soul}\setul{0.3ex}{}  % underline stuff
\usepackage{cancel}               % strikethrough stuff
\usepackage{bm}                   % bold in math mode
\usepackage{tikz}                 % used here for image absolute positioning
\usepackage{multimedia}           % for including mp4 files, see https://github.com/iblech/mathezirkel-kurs/blob/master/vierdimensionale-geometrie/animated-gif-in-latex.md



\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

% \usecolortheme{orchid}
% \usecolortheme{seahorse}
% \definecolor{mypurple}{RGB}{150,0,255}
\setbeamercolor{structure}{fg=Rhodamine}
% \definecolor{myred}{RGB}{150,0,0}
% \setbeamercolor*{title}{bg=myred,fg=white}
% \setbeamercolor*{titlelike}{bg=myred,fg=white}
% \setbeamercolor{frame}{bg=black}



\useinnertheme{rectangles}

\newcommand{\bignumber}[1]{
  \renewcommand{\insertenumlabel}{#1}\scalebox{1.5}{\usebeamertemplate{enumerate item}}
}
\newcommand{\titlecolumn}[4]{
  \begin{column}{0.24\textwidth}
    \centering
    \bignumber{#1}

    \hil{#2}
    \medskip

    \includegraphics[width=0.95\linewidth]{#3}
  \end{column}
}

\newcommand{\bv}{p}
\newcommand{\bif}[1]{\widetilde{#1}_{\bv}}
\newcommand{\bifideal}[1]{{#1}^*_{\bv}}
\newcommand{\n}{\operatorname{n}}
\newcommand{\bwinding}{\omega_{em}}
\newcommand{\J}[1]{\mathcal{J}_{#1}}
\newcommand{\ints}[2]{n_{#1 \# #2}}




\setbeamertemplate{frametitle}{%
  \vskip0.7em%
  \leavevmode%
  \begin{beamercolorbox}[dp=1ex,center]{}%
      \usebeamercolor[fg]{item}{\textbf{{\Large \insertframetitle}}}
  \end{beamercolorbox}%
}


\setbeamertemplate{headline}{%
  \begin{beamercolorbox}[wd=\paperwidth,ht=2.25ex]{}%
    \insertsectionnavigationhorizontal{\paperwidth}{}{}%
  \end{beamercolorbox}%
  \vskip0pt%
}

\setbeamertemplate{footline}{%
  \begin{beamercolorbox}[wd=\paperwidth,ht=2.25ex,dp=1ex,right,rightskip=1mm,leftskip=1mm]{}%
    % \inserttitle
    \hfill
    \insertframenumber\,/\,\inserttotalframenumber
  \end{beamercolorbox}%
  \vskip0pt%
}


\newcommand{\hil}[1]{{\usebeamercolor[fg]{item}{\textbf{#1}}}}
\newcommand{\hilm}[1]{{\usebeamercolor[fg]{item}{\boldsymbol{#1}}}}

\newcommand{\hilmwrong}[1]{{\usebeamercolor[violet]{}{\boldsymbol{#1}}\usebeamercolor[fg]{item}}}



\newcommand{\kimjplusformula}{
  $$ J^+ (\alpha) = \begin{cases}
    1 - k - kl - l^2 ,          &\text{if \( e \in I_{\text{retro}} \)} \\
    1 - k + kl - l^2 ,          &\text{if \( k > l \) and \( e \in I_{\text{direct}} \)} \\
    1 - k + kl - l^2 ,          &\text{if \( k < l \) and \( e \in I_{\text{direct}}^2 \)} \\
    1 - k - 2k^2 + 3kl - l^2 ,  &\text{if \( k < l \) and \( e \in I_{\text{direct}}^1 \)}
  \end{cases} $$
}
\newcommand{\kimjoneformula}{
  $$
  \J{1}(\alpha) = \begin{cases}
    1 - k - \frac{3}{2}k^2 + 2kl - \frac{l^2}{2} ,  &\text{if \( k < l \) and \( e \in I_{\text{direct}}^1 \)} \\
    1 - k + \frac{k^2}{2} - \frac{l^2}{2} ,         &\text{else}
  \end{cases}
  $$
}
\newcommand{\kimjtwoformulalasttwolinesincorrect}{
  1 - k - \frac{1}{2}kl - \frac{1}{2}l^2 ,       &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
  1 - k - k^2 + \frac{3}{2}kl - \frac{1}{2}l^2 , &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^1 \)}
}
\newcommand{\kimjtwoformulalasttwolinesincorrecthil}{
  \hilmwrong{1 - k - \frac{1}{2}kl - \frac{1}{2}l^2} ,       &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
  \hilmwrong{1 - k - k^2 + \frac{3}{2}kl - \frac{1}{2}l^2} , &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^1 \)}
}
\newcommand{\kimjtwoformulalasttwolinescorrecthil}{
  \hilm{1 - k + \frac{k^2}{4} - \frac{l^2}{4}} ,       &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
  \hilm{1 - k - \frac{3}{4}k^2 + kl - \frac{l^2}{4}} , &\text{if \( \omega_0 (\alpha) \) even, \( k < l \) and \( e \in I_{\text{direct}}^1 \)}
}
\newcommand{\kimjtwoformula}[1]{
  $$ \J{2}(\alpha) = \begin{cases}
    (k - 1)^2 - l^2 ,                             &\text{if \( \omega_0 (\alpha) \) odd, \( k > l \)} \\
    (k - 1)^2 - l^2 ,                             &\text{if \( \omega_0 (\alpha) \) odd, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
    1 - 2k - 3k^2 + 4kl - l^2 ,                   &\text{if \( \omega_0 (\alpha) \) odd, \( k < l \) and \( e \in I_{\text{direct}}^1 \)} \\
    1 - k + \frac{k^2}{4} - \frac{l^2}{4} ,       &\text{if \( \omega_0 (\alpha) \) even, \( k > l \)} \\

    #1
  \end{cases} $$
}
\newcommand{\kimjtwoformulahilcases}{
  $$ \J{2}(\alpha) = \begin{cases}
    (k - 1)^2 - l^2 ,                             &\text{if \( \omega_0 (\alpha) \) \textbf{odd}, \( k > l \)} \\
    (k - 1)^2 - l^2 ,                             &\text{if \( \omega_0 (\alpha) \) \textbf{odd}, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
    1 - 2k - 3k^2 + 4kl - l^2 ,                   &\text{if \( \omega_0 (\alpha) \) \textbf{odd}, \( k < l \) and \( e \in I_{\text{direct}}^1 \)} \\
    1 - k + \frac{k^2}{4} - \frac{l^2}{4} ,       &\text{if \( \omega_0 (\alpha) \) \textbf{even}, \( k > l \)} \\

    1 - k + \frac{k^2}{4} - \frac{l^2}{4} ,       &\text{if \( \omega_0 (\alpha) \) \textbf{even}, \( k < l \) and \( e \in I_{\text{direct}}^2 \cup I_{\text{retro}} \)} \\
    1 - k - \frac{3}{4}k^2 + kl - \frac{l^2}{4} , &\text{if \( \omega_0 (\alpha) \) \textbf{even}, \( k < l \) and \( e \in I_{\text{direct}}^1 \)}
  \end{cases} $$
}


\newcommand{\formelnfuerkreisbifurkationen}{
  \vspace*{-2.5em}
  \begin{equation*}
    J^+(q) = 1 - \hilm{\bv}^2 + \hilm{n_{q}}
  \end{equation*}
  \vspace*{-4em}

  \begin{equation*}
    \J{1}(q) = 1 - \frac{\hilm{\bv}^2}{2} + \hilm{n_{q}}
  \end{equation*}
  \vspace*{-4em}

  \begin{equation*}
    \J{2}(q) = \begin{cases}
      1 - \hilm{\bv}^2 + 2 \hilm{n_{q}} ,    &\text{\( \hilm{\bv} \) odd} \\
      1 - (\textstyle\frac{\hilm{\bv}}{2})^2 + \hilm{\nu_{q}} ,   &\text{\( \hilm{\bv} \) even}
    \end{cases}
  \end{equation*}
  \vspace*{-1em}

  \hspace*{-0.3em}schreibe $p$-Kreisbifurkation $\bif{S^1}$ als $\hilm{q}$

  $\rightsquigarrow$ brauchen nur $\hilm{p}$ und $\hilm{n_{\alpha}}$ vom $T_{k, l}$-type Orbit $\hilm{\alpha}$,\\
  \hspace*{-0.3em}um die Invarianten zu berechnen.

  \begin{tabular}{ |c|c|c| }
    \hline
    ~             & $e \in I_{\text{direct}}^1$ & $e \in I_{\text{retro}}$ \\
    \hline
    $\bv$         & $| k - l |$                 & $k + l$ \\
    \hline
    $n_{\alpha}$  & $k (| k - l |) - k$         & $k (k + l) - k$ \\
    \hline
  \end{tabular}
}



%%% start bunderline
\usepackage{scalerel,stackengine}
\def\buthickness{1pt}
\def\budefaultcolor{black}
\makeatletter
\newcommand\bunderline[1][\budefaultcolor]{\def\bucolor{#1}\bunderlineaux}
\newcommand\bunderlineaux[2][\buthickness]{%
  \ThisStyle{%
  \ifmmode%
    \setbox0=\hbox{\m@th$\SavedStyle#2$}
    \stackunder[2pt]{\copy0}{\textcolor{\bucolor}{\rule{\wd0}{#1}}}%
  \else%
    \xdef\butmpthickness{#1}%
    \prebunderlinewords#2 \endarg%
  \fi%
}}
\def\prebunderlinewords#1 #2\endarg{%
  \ifx\endarg#2\endarg\def\wdaugment{0pt}\else\def\wdaugment{.8ex}\fi%
  \bunderlinewords#1 #2\endarg%
}
\def\bunderlinewords#1 #2\endarg{%
    \setbox0=\hbox{#1\strut}%
    \stackengine{0pt}{\copy0}{\textcolor{\bucolor}{%
      \smash{\rule{\dimexpr\wd0+\wdaugment\relax}{\butmpthickness}}}}{U}{c}{F}{T}{S}%
    \ifx\endarg#2\endarg\def\next{}\else\ \def\next{\bunderlinewords#2\endarg}\fi\next%
}
\newcommand\buonslide[1][black]{\def\butmpcolor{#1}\buonslideauxA}
\newcommand\buonslideauxA[1][\buthickness]{\def\butmpthickness{#1}\buonslideauxB}
\def\buonslideauxB<#1>#2{\onslide<#1>{%
  \rlap{\bunderline[\butmpcolor][\butmpthickness]{\phantom{#2}}}}#2}
\makeatother
% \useinnertheme{default}
\beamertemplatenavigationsymbolsempty
%%% end bunderline



\begin{document}

\addtocounter{framenumber}{-1}



\begin{frame}[c]
  \centering

  \hil{Bifurcations and $J^+$-like invariants \\in the rotating Kepler problem} \\
  \emph{-- Kolloquium zur Masterarbeit --}
  \vfill

  \only<1>{
    \includegraphics[height=11.5em]{../cover-image.pdf}
  }

  \only<2>{
    \begin{columns}
      \small

      \titlecolumn{1}
      {Wiederholung Grundlagen \& Bachelorarbeit}
      {kol-title-master-1}

      \titlecolumn{2}
      {Task 1: Beweis mit Bifurkati- onsformeln}
      {kol-title-master-2}

      \titlecolumn{3}
      {Fallunter-scheidung \& Birkhoff}
      {kol-title-master-3}

      \titlecolumn{4}
      {Task 2: Formeln für Two-Center Invarianten}
      {ghost} % TODO bild

    \end{columns}
  }

  \vfill
  \scriptsize
  Alexander Mai, Universität Augsburg \\
  01. Juli 2024
\end{frame}




\section{Wiederholung}

% \begin{frame}{}
%   \vspace*{1.0em}
%   Wir betrachten \\
%   \vspace*{-0.5em}

%   \begin{center}
%     \textbf{
%       \bunderline[RubineRed]{generische} 
%       \bunderline[BurntOrange]{reguläre} 
%       \bunderline[ForestGreen]{geschlossene} 
%       \bunderline[Cyan]{ebene} 
%       % \bunderline[Brown]{Kurven}
%       Kurven.
%     }
%   \end{center}

%   \vspace{-0.2em}
%   \begin{block}{}
%     \vspace*{-0.16em}
%     Bild $K \subset \bunderline[Cyan]{\mathbb{C}}$ einer \bunderline[BurntOrange]{glatten} Abbildung
%     \vspace*{-0.08em}
%     $$ q: \bunderline[ForestGreen]{S^1} \to \bunderline[Cyan]{\mathbb{C}} ,$$
%     mit $\bunderline[BurntOrange]{q'(s) \neq 0 \,\, \forall s \in S^1}$ \\
%     \vspace*{-0.2em}
%     und \bunderline[RubineRed]{alle Schnittpunkte sind transversale Doppelpunkte.}
%   \end{block}

%   \vskip 0pt plus 1filll

%   \begin{columns}
%     \begin{column}{0.65\textwidth}
%       \vspace*{-7.5em}
%       \begin{itemize}
%         \item keine Unterbrechung
%         \item keine Ecken
%         \item höchstens isolierte Doppelpunkte
%         \item bezeichnen wir als \hil{Immersion}
%       \end{itemize}
%     \end{column}
%     \begin{column}{0.4\textwidth}
%       \includegraphics[height=7em]{kol-kurve4.pdf}
%     \end{column}
%   \end{columns}
% \end{frame}


\begin{frame}{Die 3 \glqq{}Desaster\grqq}
  \centering

  \hil{Direkte Selbsttangente} \\
  \includegraphics[height=3.5em]{jselfdirect.pdf}

  \hil{Inverse Selbsttangente} \\
  \includegraphics[height=3.5em]{jselfinverse.pdf}

  \hil{Dreifachpunkte} \\
  \includegraphics[height=3.5em]{jtrip.pdf}

  % die einzigen \hil{nicht-generischen} Momente während regulärer Homotopie einer Immersion (i.A. isolierte Momente)
\end{frame}



% \begin{frame}{Definition $\bm{J^+}$-Invariante}
%   \centering
%   \includegraphics[height=4em]{kol-standardcurves3.pdf}
%   \begin{itemize}\itemsep=0.2em
%     \item Abbildung\quad $\{ \text{Immersionen} \} \longrightarrow 2 \mathbb{Z}$
%     \item $K_0 \longmapsto 0$
%     \item $K_j \longmapsto -2 (|j| - 1)$
%     \item \hil{orientierungsunabhängig}
%     \item pos. (neg.) \hil{dst} erhöht $J^+$ um $2$ ($-2$)
%     \item additiv unter \hil{verbundener Summe}
%   \end{itemize}
% \end{frame}




\begin{frame}{$\bm{\bv}$-Bifurkationen}
  \centering
  \vfill
  
  \includegraphics[height=15em]{kol-bifs4.pdf}
  \vfill
\end{frame}





% \begin{frame}{Rückblick Beweis: Untere Schranke (3/4)}
%   \vspace*{-1.5em}
%   $$- \Gamma_V(\bifideal{K}) + \mathcal{D}_V(\bifideal{K}) = k^2 \left(- \Gamma_V(K) + \mathcal{D}_V(K) \right)$$

%   Partitionierung von $\Gamma_{\bifideal{K}}$ und $\mathcal{D}_{\bifideal{K}}$:

%   \begin{itemize}
%     \item $\Gamma_{\bifideal{K}} \,\, = \, \bunderline[BurntOrange]{\Gamma^{\text{old}}_{\bifideal{K}}} \cup \, \bunderline[RubineRed]{\Gamma^{\text{square}}_{\bifideal{K}}} \, \cup \bunderline[Cyan]{\Gamma^{\text{stripe}}_{\bifideal{K}}}$
%     \item $\mathcal{D}_{\bifideal{K}} = \mathcal{D}^{\text{old}}_{\bifideal{K}} \cup \mathcal{D}^{\text{square}}_{\bifideal{K}} \cup \mathcal{D}^{\text{stripe}}_{\bifideal{K}} \cup \mathcal{D}^{\text{neat}}_{\bifideal{K}}$
%   \end{itemize}

%   \begin{center}
%     \vspace*{-1em}
%     \includegraphics[height=9.5em]{kol-compsteaser.pdf}
%   \end{center}\vspace*{-15em}
% \end{frame}





\begin{frame}{Rückblick Beweis: Untere Schranke (4/4)}
  \vspace*{-1.3em}
  \begin{itemize}\itemsep=0.5em
    \item $\Gamma^{\text{old}}_V (\bifideal{K}) \quad = k^2 \cdot \Gamma_V (K)$ , \quad\quad $\mathcal{D}^{\text{old}}_V (\bifideal{K}) \quad\!\! = k^2 \cdot \mathcal{D}_V (K)$
    \item $\bunderline[RubineRed]{\Gamma^{\text{square}}_V (\bifideal{K}) = \mathcal{D}^{\text{square}}_V (\bifideal{K})}$
    \item $\bunderline[Cyan]{\Gamma^{\text{stripe}}_V (\bifideal{K}) \, = \mathcal{D}^{\text{stripe}}_V (\bifideal{K}) + \mathcal{D}^{\text{neat}}_V (\bifideal{K})}$
  \end{itemize}

  \vspace*{-0.5em}
  \begin{center}
    \includegraphics[height=13.5em]{kol-matchab.pdf}
  \end{center}
\end{frame}





\section{Task 1}

% \begin{frame}
%   \centering
%   \includegraphics[height=\textheight]{kol-title-master-2.pdf}
% \end{frame}


\begin{frame}{Zu überprüfende Formeln}
  \centering
  \vspace*{-2em}
  \footnotesize

  \kimjplusformula

  \kimjoneformula

  \kimjtwoformula{\kimjtwoformulalasttwolinesincorrect}
\end{frame}


\begin{frame}
  \centering
  \includegraphics[height=0.9\textheight]{kol-kimpaper-4x4orbitstkl-5k2l.png}

  $\hilm{k = 5, l = 2}$
\end{frame}


\begin{frame}{Zweites Keplersches Gesetz}
  \centering
  \only<1>{
    \movie[width=12cm,height=6.75cm,autostart,loop,poster,showcontrols]{}{img/kolloquium/nasa-kepler2.mp4}
  }
  \only<2>{
    \includegraphics[width=12cm]{nasa-kepler2c.jpg}
  }
\end{frame}


\begin{frame}{Rotierte Kepler-Ellipsen}
  \centering
  \only<2>{
    \vspace*{-4em}
    \movie[width=1.2\textheight,height=0.9\textheight,autostart,loop,poster,showcontrols]{}{img/kolloquium/tklrotated/tklrot-ecc02-5k2l-250speed.mp4}

    $\hilm{k = 5, l = 2, e = 0.2}$
  }

  \only<3>{
    \vspace*{-4em}
    \movie[width=1.2\textheight,height=0.9\textheight,autostart,loop,poster,showcontrols]{}{img/kolloquium/tklrotated/eccall-5k2l-d-crop.mp4}

    $\hilm{k > l}: \hilm{k = 5, l = 2, e \in \{0, \frac{1}{10}, \frac{2}{10}, ..., \frac{9}{10}, 1\}}$
  }

  \only<4>{
    \includegraphics[height=0.75\textheight]{kol-tklorbits-kbiggerl-ints.pdf}

    $\hilm{k = 5, l = 2}$
  }
\end{frame}



\begin{frame}{Kreisbifurkationen?}
  \centering
  \includegraphics[height=0.75\textheight]{kol-tklorbits-ksmallerl-ints-spotlight.pdf}

  $\hilm{k = 3, l = 5}$
\end{frame}



% \begin{frame}{Formeln für Kreisbifurkationen}
%   \centering
%   \formelnfuerkreisbifurkationen
% \end{frame}


% \begin{frame}{Erinnerung: Intervalle $I_{\text{direct}}^1 , I_{\text{direct}}^2$ und $I_{\text{retro}}$}
%   \centering
%   \includegraphics[height=0.75\textheight]{kol-tklorbits-kbiggerl-ints.pdf}

%   $\hilm{k = 5, l = 2}$
% \end{frame}


\begin{frame}{Pizza, aber wie viele Stücke?}
  \centering
  \only<1>{
    \vfill
    \includegraphics[width=\textwidth]{kol-kimpaper-dpsslices.png}
    \vfill
  }
  \only<2>{
    \includegraphics[height=0.8\textheight]{kol-kimpaper-polygonapprshaded.png}
  }
\end{frame}



\begin{frame}{Erste Aufgabe der Masterarbeit}
  \centering
  Überprüfe, mithilfe der Bifurkationsformeln aus der Bachelorarbeit, die Formeln für die $J^+$-, $\J{1}$- und $\J{2}$-Invarianten für die \emph{$T_{k, l}$-type Orbits} aus dem Paper

  \textbf{$J^+$-like invariants of periodic orbits of the second kind in the restricted three body problem}

  (2018, Joontae Kim und Seongchan Kim)

  \huge
  $\rightsquigarrow$ \color{Red} \textbf{GESCHEITERT\pause .\pause.\pause?}

\end{frame}


\begin{frame}{Formeln überprüft und Fehler entdeckt}
  \centering
  \vspace*{-2em}
  \footnotesize

  \kimjplusformula

  \kimjoneformula

  \kimjtwoformula{\kimjtwoformulalasttwolinescorrecthil}
\end{frame}


\begin{frame}{Fehler im Paper}
  \centering
  \vfill
  \includegraphics[width=\textwidth]{kol-kimpaper-prooferror.png}

  \hfill ... a wrong result :-(
  \vfill
\end{frame}


\begin{frame}{}
  \centering
  \includegraphics[height=20em]{meme-tkl-bif-tabletennis.jpg}
\end{frame}








\section{Fallunterscheidung \& Birkhoff}

% \begin{frame}
%   \centering
%   \includegraphics[height=\textheight]{kol-title-master-3.pdf}
% \end{frame}



\begin{frame}{Levi-Civita-Regularisierung}
  \centering
  \vfill
  \includegraphics[height=0.8\textheight]{kol-squaringinverserip.pdf} % erzählen: Verbindung zur Windungszahl bzw. dass die Windungszahl genau übereinstimmt mit Anzahl wie oft im Urbild zwischen den zwei Halbebenen (Fächern) gewechselt wird.
  \vfill
\end{frame}



\begin{frame}{Birkhoff-Regularisierung}
  \centering
  \vfill
  \includegraphics[width=\textwidth]{cover-image.pdf}

  $ B : \mathbb{C}^* \to \mathbb{C}, \quad B(z) = \frac{1}{2} \left( z + \frac{1}{z} \right), \quad B^{-1}(y) = y \pm \sqrt{y^2 - 1} $
  \vfill
\end{frame}


\begin{frame}{}
  \centering
  \vfill
  \only<1>{
    \includegraphics[width=\textwidth]{kol-birkhoffinvcoords-quadrant.png}
  }
  \only<2>{
    \includegraphics[width=\textwidth]{kol-birkhoffinvcoords-ylines.png}
  }
  $ B : \mathbb{C}^* \to \mathbb{C}, \quad B(z) = \frac{1}{2} \left( z + \frac{1}{z} \right), \quad B^{-1}(y) = y \pm \sqrt{y^2 - 1} $
  \vfill
\end{frame}




\section{Task 2}

% \begin{frame}{Zweite Aufgabe der Masterarbeit}
%   \begin{center}
%     Finde und beweise Bifurkationsformeln für die two-center Invarianten $\J{0} ,$ $\J{E} ,$ $\J{M}$ und $\J{E, M}$ aus dem Paper

%     \textbf{$J^+$-invariants for planar two-center Stark-Zeeman systems}

%     (2019, Cieliebak, Frauenfelder and Zhao)
%   \end{center}
% \end{frame}



\begin{frame}{Bifurkationsformeln}
  \centering
  \vspace*{-2em}

  \begin{equation*}
    \J{0}(\bif{K}) = \bv^2 \J{0}(K) - (\bv^2 - 1) + (n_{\bif{K}} - n_K \bv^2)
  \end{equation*}
  \vspace*{-2em}

  {
    \only<2>{\footnotesize}
    \begin{equation*}
      \J{E} (\bif{K}) = \begin{cases}
        \bv^2 \J{E}(K) - (\bv^2 - 1) + (n_{\bif{K}} - n_K \bv^2) ,    &\text{if \( \omega_E(K) \) even} \\
        \bv^2 \J{E}(K) - (\bv^2 - 1) + 2 (n_{\bif{K}} - n_K \bv^2) ,  &\text{if \( \omega_E(K) \) odd, \( \bv \) odd} \\
        (\textstyle\frac{\bv}{2})^2 \J{E}(K) - ((\textstyle\frac{\bv}{2})^2 - 1) + (\nu_{\bif{K}} - n_K \textstyle\frac{\bv^2}{2}) , &\text{if \( \omega_E(K) \) odd, \( \bv \) even}
      \end{cases}
    \end{equation*}
  }
  \vspace*{-2em}

  \only<2>{\tiny}
  \begin{equation*}
    (\J{E, M}, \n) (\bif{K}) =
    \begin{cases}
      \bv^2 (\J{E, M}, \n) (K) - (\bv^2 - 1) + (n_{\bif{K}} - n_K \bv^2) \pmod{2 \bv \n(K)} ,
        &\text{if \( \bwinding (K) \) even} \\
      \bv^2 (\J{E, M}, \n) (K) - (\bv^2 - 1) + 2 (n_{\bif{K}} - n_K \bv^2) \pmod{2 \bv \n(K)} ,
        &\text{\( \bwinding (K) \) odd, \( \bv \) odd} \\
      (\frac{\bv}{2})^2 (\J{E, M}, \n) (K) - ((\frac{\bv}{2})^2 - 1) + (\nu_{\bif{K}} - n_K \frac{\bv^2}{2}) \pmod{2 \frac{\bv}{2} \n(K)} ,
        &\text{\( \bwinding (K) \) odd, \( \bv \) even}
    \end{cases}
  \end{equation*}

  \vfill
\end{frame}



\begin{frame}{}
  \centering
  % When you realize that an extra ``mod'' \\
  % has to appear in the bifurcation formula
  \includegraphics[width=10cm]{whenyou.png}
  \movie[width=10cm,height=5.625cm,autostart,loop,poster]{}{img/kolloquium/bbb-muted.mp4}
  {\tiny
  \begin{equation*}
    (\J{E, M}, \n) (\bif{K}) =
    \begin{cases}
      \bv^2 (\J{E, M}, \n) (K) - (\bv^2 - 1) + (n_{\bif{K}} - n_K \bv^2) \pmod{2 \bv \n(K)} ,
        &\text{if \( \bwinding (K) \) even} \\
      \bv^2 (\J{E, M}, \n) (K) - (\bv^2 - 1) + 2 (n_{\bif{K}} - n_K \bv^2) \pmod{2 \bv \n(K)} ,
        &\text{\( \bwinding (K) \) odd, \( \bv \) odd} \\
      (\frac{\bv}{2})^2 (\J{E, M}, \n) (K) - ((\frac{\bv}{2})^2 - 1) + (\nu_{\bif{K}} - n_K \frac{\bv^2}{2}) \pmod{2 \frac{\bv}{2} \n(K)} ,
        &\text{\( \bwinding (K) \) odd, \( \bv \) even}
    \end{cases}
  \end{equation*}}
\end{frame}



\begin{frame}
  \centering
  \vfill
  % intentionally blank slide
  \vfill
\end{frame}








\section{Extra}


\begin{frame}{Danke fürs Zuhören! :-)}
  \centering

  \vspace*{2em}
  Vortragsfolien, Masterarbeit und alle Dateien dazu zur\\freien Verwendung auf gitlab:\\
  \textbf{gitlab.com/CptMaister/masterthesis}

  \,\\ \,\\
  Urbildimmersionen für Birkhoff- oder Levi-Civita-Regularisierung selbst testen:\\
  \textbf{complex-fibers.org}
\end{frame}





% \addtocounter{framenumber}{-1}
\end{document}
