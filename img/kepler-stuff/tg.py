from turtle import *
import time, math

import numpy as np
from PyAstronomy import pyasl
import matplotlib.pylab as plt

import canvasvg

from thread_serializer import ThreadSerializer
from turtle_thread import TurtleThread

ctrl = ThreadSerializer() 


semiMajorAxisLength = 1.
timeUnitSingleEllipseRun = 2

# see https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/keplerOrbit.html#example-calculating-the-orbit <- slightly adjusted
def keplerEllipse(
    numberOfEllipsePoints,
    eccentricity
  ):
  """
  Get equally time-spaced points of a Kepler ellipse.

  The ellipse run's velocity obeys Kepler's second planetary law: "A line segment joining a planet and the Sun sweeps out equal areas during equal intervals of time." This is achieved with the PyAstronomy packages. More info on the calculations here: https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/keplerOrbit.html

  Parameters
  ----------
  numberOfEllipsePoints : int
      The number of points per ellipse run (they are then connected by straight lines).
  eccentricity : int, between 0 and 1
      Eccentricity of the ellipse, where 0 is a circle and 1 is a line.
  """

  keplerEllipse = pyasl.KeplerEllipse(semiMajorAxisLength, timeUnitSingleEllipseRun, e=eccentricity)

  # secondFocusPoint = keplerEllipse.xyzFoci()[1]
  # centerOfEllipse = keplerEllipse.xyzCenter()

  timeMoments = np.linspace(0, timeUnitSingleEllipseRun, numberOfEllipsePoints, endpoint=False)

  # Calculate the orbit position at the given moments.
  # This returns a numpy ndarray with the same amount of entries (or rows) as timeMoments has entries, each with 3 coordinates (x, y, z).
  xyzPositions = keplerEllipse.xyzPos(timeMoments)

  # omit the z-coordinates (they are always zero anyway)
  xyPositions = np.delete(xyzPositions, 2, 1)
  # flip the y-coordinates (so that the rotated orbits look more like the ones from the kim paper)
  xyPositions[:, 1] *= -1

  return xyPositions


def rotatedKeplerEllipse(
    numberOfEllipsePoints,
    eccentricity,
    numberOfEllipseRuns,
    numberOfSystemRotations,
    directNotRetrograde=True
  ):
  """
  Get equally time-spaced points of a rotated Kepler ellipse, where the total rotations take as much time as the total ellipse runs.

  The rotation velocity is constant, while an ellipse run's velocity obeys Kepler's second planetary law.

  Parameters
  ----------
  numberOfEllipsePoints : int
      The number of points per ellipse run (they are then connected by straight lines).
  eccentricity : int, between 0 and 1
      Eccentricity of the ellipse, where 0 is a circle and 1 is a line.
  numberOfEllipseRuns: int, positive
  numberOfSystemRotations: int, non-negative
      Should be coprime to numberOfEllipseRuns, if not zero.
  """

  keplerPositions = keplerEllipse(numberOfEllipsePoints, eccentricity)
  multipleRunsKeplerPositions = keplerPositions
  for i in range(1, numberOfEllipseRuns):
    multipleRunsKeplerPositions = np.concatenate((multipleRunsKeplerPositions, keplerPositions), axis=0)
  
  theta = 2 * np.pi * numberOfSystemRotations / len(multipleRunsKeplerPositions) # the rotation in radians, accumulating per point

  if directNotRetrograde is False:
    theta *= -1

  xyPositions = multipleRunsKeplerPositions
  for index, xyPos in enumerate(xyPositions):
    rotatedPosition = np.dot(xyPos, getRotationMatrixTransposed(theta * index))
    xyPositions[index] = rotatedPosition

  return xyPositions


def getRotationMatrixTransposed(theta):
  cos = np.cos(theta)
  sin = np.sin(theta)
  # return np.array([[cos, -sin], [sin, cos]]) # normal rotation matrix
  return np.array([[cos, sin], [-sin, cos]]) # transposed rotation matrix


def drawPolygon(turtle, points, centerX, centerY, turtleColor="#000000", stampAfterPoints=0):
  """
  Let a turtle draw points, connected by straight lines.

  Parameters
  ----------
  turtle : Turtle
      The turtle.
  points : ndarray
      List of x-y-coordinate pairs. Should be at least two points.
  centerX, centerY: number
      Take these coordinates as origin for the points.
  turtleColor: valid color name or hex
      Turtle color.
  stampAfterPoints: boolean
      Stamp the current position after this many visited polygon points.
  """

  if len(points) < 2:
    raise Exception("Not enough points, len(points) is: " + str(len(points)))
  
  turtle.color(turtleColor)

  turtle.penup()
  
  firstPoint = points[0]
  turtle.goto(centerX + firstPoint[0], centerY + firstPoint[1])
  turtle.pendown()
  if stampAfterPoints > 0:
      turtle.stamp()

  for index, point in enumerate(points[1:]):
    turtle.goto(centerX + point[0], centerY + point[1])
    if stampAfterPoints > 0 and index > 0 and (index + 1) % stampAfterPoints == 0:
      turtle.stamp()
  
  turtle.goto(centerX + firstPoint[0], centerY + firstPoint[1])
  
  turtle.penup()


def createTurtle():
  # check here for more information on turtle: https://docs.python.org/3/library/turtle.html
  newTurtle = Turtle()
  newTurtle.hideturtle()
  newTurtle.shape("circle")
  newTurtle.speed(0)
  newTurtle.pensize(5)
  newTurtle.penup()
  return newTurtle


def drawStuff(turtle, points, centerX, centerY, stampAfterPoints=0):
  drawPolygon(turtle, points, centerX, centerY, "black", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "forest green", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "magenta", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "navy", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "dark orange", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "white", stampAfterPoints)
  drawPolygon(turtle, points, centerX, centerY, "black", stampAfterPoints)


# setup of values is here

numberOfEllipsePoints = 100 # equally spaced in time, but not necessarily in distance, because of Kepler's second law
numberOfEllipseRuns = 5
numberOfSystemRotations = 2
directNotRetrograde = True

scalePositions = 80
# stampAfterPoints = 0
stampAfterPoints = numberOfEllipsePoints

centerXDistance = 300
centerXAmount = 4
centerXCoords = list(map(
  lambda i: 0 - ((centerXAmount - 1) / 2) * centerXDistance + i * centerXDistance,
  range(centerXAmount)
))
centerYDistance = -300
centerYAmount = 3
centerYCoords = list(map(
  lambda i: 0 - ((centerYAmount - 1) / 2) * centerYDistance + i * centerYDistance,
  range(centerYAmount)
))
eccentricities = [
  # 0.0,
  # 0.1,
  0.2,
  # 0.3,
  # 0.4,
  # 0.5,
  # 0.6,
  # 0.7,
  # 0.8,
  # 0.9,
  # 0.999
]
# eccentricities.reverse()

turtlesAndTheirStuff = list(map(
  lambda i: {
    "turtle": createTurtle(),
    "centerX": centerXCoords[i % len(centerXCoords)],
    "centerY": centerYCoords[math.floor(i / len(centerXCoords))],
    "eccentricity": eccentricities[i]
  },
  range(len(eccentricities)))
)

screen = turtlesAndTheirStuff[0]["turtle"].getscreen()


for stuff in turtlesAndTheirStuff:
  turtle = stuff["turtle"]
  eccentricity = stuff["eccentricity"]
  centerX = stuff["centerX"]
  centerY = stuff["centerY"]

  turtle.penup()
  turtle.goto(centerX, centerY)
  turtle.showturtle()
  turtle.stamp()

  # screen.tracer(0) # use this line to get the result faster, without seeing every turtle step

  positions = rotatedKeplerEllipse(numberOfEllipsePoints, eccentricity, numberOfEllipseRuns, numberOfSystemRotations, directNotRetrograde)
  positions *= scalePositions

  tt = TurtleThread(ctrl, turtle, target=drawStuff, args=[positions, centerX, centerY, stampAfterPoints])
  tt.daemon = True
  tt.start()
  # drawStuff(turtle, positions, centerX, centerY, stampAfterPoints)

  # turtle.hideturtle()
  # screen.update()

time.sleep(1) # used for better recordings of the animation
ctrl.run_forever(1)

## use these lines to save the result as svg
# canv = screen.getcanvas()
# canvasvg.saveall("tg.svg", canv, margin=15)

screen.mainloop() # use this line to keep the canvas open here (code will not finish properly)
