# from __future__ import print_function, division
import numpy as np
from PyAstronomy import pyasl
import matplotlib.pylab as plt



# see https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/keplerOrbit.html#example-calculating-the-orbit <- slightly adjusted


semiMajorAxisLength = 1.
eccentricity = 0.6
timeUnitSingleEllipseRun = 2
numberOfEllipsePoints = 20 # equally spaced in time, but not necessarily in distance, because of Kepler's second law


keplerEllipse = pyasl.KeplerEllipse(semiMajorAxisLength, timeUnitSingleEllipseRun, e=eccentricity)

# firstFocusPoint = keplerEllipse.xyzFoci()[0] # we assume this to be (0,0) anyway, use this for debugging reasons if someone plays around with this code and breaks stuff
secondFocusPoint = keplerEllipse.xyzFoci()[1]
centerOfEllipse = keplerEllipse.xyzCenter()


timeMoments = np.linspace(0, timeUnitSingleEllipseRun, numberOfEllipsePoints, endpoint=False)

# Calculate the orbit position at the given moments.
# This returns a numpy ndarray with the same amount of entries (or rows) as timeMoments has entries, each with 3 coordinates (x, y, z).
xyzPositions = keplerEllipse.xyzPos(timeMoments)

# # ndarray shape of positions
# print("Shape of positions: ", xyzPositions.shape)

# omit the z-coordinates (they are always zero anyway)
xyPositions = np.delete(xyzPositions, 2, 1)

# flip the y-coordinates (so that the rotated orbits look more like the ones from the kim paper)
xyPositions[:, 1] *= -1

print(xyzPositions)
print(xyPositions)

# # x, y, and z coordinates for 50th time point
# print("x, y, z for 50th point: ", pos[50, ::])

# # More useful stuff, check here for more: https://pyastronomy.readthedocs.io/en/latest/pyaslDoc/aslDoc/keplerOrbitAPI.html#PyAstronomy.pyasl.KeplerEllipse
# radius(t[, E])              Calculate the orbit radius.
# xyzVel(t)                   Calculate orbit velocity.
# xzCrossingTime()            Calculate times of crossing the xz-plane.
# yzCrossingTime([ordering])  Calculate times of crossing the yz-plane.

# # Calculate velocity on orbit
# vel = keplerEllipse.xyzVel(timeMoments)

# # Find the nodes of the orbit (Observer at -z)
# ascn, descn = keplerEllipse.xyzNodes_LOSZ()

# Plot x and y coordinates of the orbit
plt.subplot(1, 1, 1)
plt.plot([0], [0], 'k+', markersize=9)
plt.plot(xyPositions[::, 0], xyPositions[::, 1], 'bp')
# first point
plt.plot([xyPositions[0, 0]], [xyPositions[0, 1]], 'rd')

# # Nodes of the orbit
# plt.plot([ascn[1]], [ascn[0]], 'go', markersize=10)
# plt.plot([descn[1]], [descn[0]], 'ro', markersize=10)

# # Plot RV
# plt.subplot(2, 1, 2)
# plt.xlabel("Time")
# plt.ylabel("Radial velocity [length/time]")
# plt.plot(t, vel[::, 2], 'r.-')
plt.show()
