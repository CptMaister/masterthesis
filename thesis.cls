\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{thesis}

\LoadClass[a4paper, bibliography=totocnumbered, listof=totocnumbered]{scrartcl}

%% packages
\RequirePackage[utf8]{inputenc}
\RequirePackage{geometry}
\RequirePackage{xcolor}
\RequirePackage{fancyhdr}
\RequirePackage[style=alphabetic, backend=biber, sorting=ynt]{biblatex}
\RequirePackage{microtype}
\RequirePackage[pdfpagelabels=true, colorlinks=true]{hyperref}
\RequirePackage{tikz}
\usetikzlibrary{arrows, calc, cd}
\RequirePackage{amsmath, amssymb, amsthm, amsfonts, mathtools}
\RequirePackage{array}
\RequirePackage{graphicx}
\RequirePackage{enumitem}
\RequirePackage{interval}
\RequirePackage{csquotes}
\RequirePackage[plain]{fancyref}
\RequirePackage{float}
\RequirePackage[tikz]{mdframed}
\RequirePackage{subcaption}
\RequirePackage{marvosym}


%% font
\RequirePackage{CrimsonPro}
% The font package uses mweights.sty which has som issues with the \normalfont command.
\let\oldnormalfont\normalfont
\def\normalfont{\oldnormalfont\mdseries}

%% themecolor
\definecolor{themecolordark}{HTML}{0B2F3A}

%% package configs
\intervalconfig{soft open fences}
\geometry{a4paper, top=27mm, left=30mm, right=27mm, bottom=35mm, headsep=10mm, footskip=12mm, twoside}
\addbibresource{bibliography/sources.bib}

% TODO finish
%% Hypersetup
\hypersetup{
    pdftitle={Bifurcations and $J^+$-like invariants in the rotating Kepler problem},
		pdfsubject={piecewise linear geometry},
		pdfauthor={Alexander Mai},
		% pdfkeywords={PL, piecewise linear, Darboux, symplectic, geometry},
		pdfkeywords={bifurcation, arnold, invariant},
		pdfcreator={pdflatex},
		pdfproducer={LaTeX},
		colorlinks=true,
		linkcolor=themecolordark,
		citecolor=themecolordark,
		urlcolor=themecolordark
}

%% Theorems
% theoremstyle
\newtheoremstyle{thesistheoremstyle}% name of the style to be used
  {0pt}% measure of space to leave above the theorem. E.g.: 3pt
  {0pt}% measure of space to leave below the theorem. E.g.: 3pt
  {\upshape}% name of font to use in the body of the theorem
  {0pt}% measure of space to indent
  {\bfseries}% name of head font
  {}% punctuation between head and body
  { }% space after theorem head; " " = normal interword space
  {\thmname{#1}\thmnumber{ #2}\textnormal{\thmnote{ (\textit{#3})}}}

\theoremstyle{thesistheoremstyle}
\newtheorem{thesisproposition}{Proposition}

% mdframed definition env
\mdfsetup{skipbelow=0}
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
innertopmargin=0,%
rightmargin=0,%
innerrightmargin=0,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=gray,%
topline=false,%
bottomline=false,%
rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{defi}{Definition}[section]

% mdframed remark env
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
rightmargin=0,%
innerrightmargin=0,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=lightgray,%
topline=false,%
bottomline=false,%
rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{remmm}[defi]{Remark}

% mdframed lemma env
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
rightmargin=0,%
innerrightmargin=0,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=darkgray,%
topline=false,%
bottomline=false,%
rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{thesislemma}[defi]{Lemma}

% mdframed corollary env
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
rightmargin=0,%
innerrightmargin=10pt,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=gray,%
topline=false,%
bottomline=false,%
% rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{thesiscorollary}[defi]{Corollary}

% mdframed proposition env
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
rightmargin=0,%
innerrightmargin=10pt,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=darkgray,%
topline=false,%
bottomline=false,%
% rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{propooo}[defi]{Proposition}

% mdframed theorem env
\newmdtheoremenv[%
outerlinewidth=2,%
leftmargin=0,%
rightmargin=0,%
innerrightmargin=10pt,%
innertopmargin=0,%
innerbottommargin=0,%
skipbelow=.4\baselineskip,%
skipabove=.4\baselineskip,%
linecolor=black,%
topline=false,%
bottomline=false,%
% rightline=false,%
linewidth=2pt,%
ntheorem=true%
]{theooo}[defi]{Theorem}

% actual theorem env

\newtheoremstyle{thesisquestionstyle}% name of the style to be used
  {0pt}% measure of space to leave above the theorem. E.g.: 3pt
  {0pt}% measure of space to leave below the theorem. E.g.: 3pt
  {\normalfont\itshape}% name of font to use in the body of the theorem
  {0pt}% measure of space to indent
  {\color{gray}\normalfont\bfseries}% name of head font
  {:}% punctuation between head and body
  {  }% space after theorem head; " " = normal interword space
  {\thmname{#1}\thmnumber{ #2}\textnormal{\thmnote{ (\textit{#3})}}}
 
\theoremstyle{thesisquestionstyle}
\newtheorem*{q}{Question}

\theoremstyle{plain}
\newtheorem*{theorem}{Theorem}
\newtheorem*{remark}{Remark}
\newtheorem*{task}{Task}

%% environments
% proofs
\def\QEDmark{\ensuremath{\square}}
\def\prf{\noindent\textbf{Proof:}}
\def\endprf{\hfill\QEDmark\vspace*{3pt}\newline}


%% pages
% pagestyle for titlepage
\fancypagestyle{thesistitlepage}{%
  \renewcommand{\headheight}{150pt}
  \chead{\Huge\textbf{\thesistitle}}
  \cfoot{}
  \renewcommand{\headrulewidth}{0pt}
  \renewcommand{\footrulewidth}{0pt}
}

% pagestyle for Intro+Abstract
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancypagestyle{intro}{%
  \fancyhead[RO, LE]{Introduction}
  \fancyhead[LO, RE]{}
  \fancyfoot[LO, RE]{\thesistitle}
  \fancyfoot[RO, LE]{\thepage}
  \cfoot{}
  \chead{}
  \renewcommand{\headheight}{15pt}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

% pagestyle for normal pages 
\renewcommand{\sectionmark}[1]{\markright{\thesection\ #1}}
\fancypagestyle{plain}{%
  \fancyhead[RO, LE]{\rightmark}
  \fancyhead[LO, RE]{}
  \fancyfoot[LO, RE]{\thesistitle}
  \fancyfoot[RO, LE]{\thepage}
  \cfoot{}
  \chead{}
  \renewcommand{\headheight}{15pt}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}


% pagestyle for TOC
\fancypagestyle{tocpage}{%
  \chead{}
  \rhead{\large\textbf{Table of Contents}}
  \lhead{}
  \cfoot{}
  \rfoot{}
  \lfoot{}	
  \renewcommand{\headheight}{15pt}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

% pagestyle for LIT
\fancypagestyle{litpage}{%
  \chead{}
  \rhead{\large\textbf{Literature}}
  \lhead{}
  \cfoot{}
  \rfoot{}
  \lfoot{}
  \renewcommand{\headheight}{15pt}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}


% pagestyle for conclusion pages 
\fancypagestyle{finpage}{%
  \fancyhead[RO, LE]{Conclusion}
  \fancyhead[LO, RE]{}
  \fancyfoot[LO, RE]{\thesistitle}
  \fancyfoot[RO, LE]{\thepage}
  \cfoot{}
  \chead{}
  \renewcommand{\headheight}{15pt}
  \renewcommand{\headrulewidth}{0.4pt}
  \renewcommand{\footrulewidth}{0.4pt}
}

%% fancyref setup
% refer to lemmas
\newcommand*{\fancyreflstlabelprefix}{lem}

\fancyrefaddcaptions{english}{%
  \providecommand*{\freflstname}{lemma}%
  \providecommand*{\Freflstname}{Lemma}%
}

\frefformat{plain}{\fancyreflstlabelprefix}{\freflstname\fancyrefdefaultspacing#1}
\Frefformat{plain}{\fancyreflstlabelprefix}{\Freflstname\fancyrefdefaultspacing#1}

%% Nice to have
% some shorthands
\let\implies\Rightarrow
\let\iff\Leftrightarrow
\newcommand{\N}{\ensuremath{\mathbb{N}}} % natural numbers
\newcommand{\Z}{\ensuremath{\mathbb{Z}}} % integers
\newcommand{\R}{\ensuremath{\mathbb{R}}} % reals
\newcommand{\Q}{\ensuremath{\mathbb{Q}}} % rational numbers
\newcommand{\K}{\ensuremath{\mathbb{K}}} % generic field
\newcommand{\C}{\ensuremath{\mathbb{C}}} % complex numbers
\newcommand{\id}{\ensuremath{\operatorname{id}}} % identity
\newcommand{\eps}{\ensuremath{\varepsilon}}
\newcommand{\adjunction}[2]{\ensuremath{#1 \left[ #2 \right]}}
\newcommand{\im}{\mathrm{i}}
\newcommand{\e}{\mathrm{e}}
\renewcommand{\Re}{\operatorname{Re}}
\renewcommand{\Im}{\operatorname{Im}}

% quotes and quotations
\renewcommand{\enquote}[1]{\textquote{#1}}
\newcommand{\ironic}[1]{\textsl{#1}}
\renewcommand{\emph}[1]{\textit{#1}}

% declare absolute value, norm, ...
\DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\DeclarePairedDelimiter{\simp}{\langle}{\rangle}

\makeatletter
\let\oldabs\abs
\def\abs{\@ifstar{\oldabs}{\oldabs*}}
\let\oldnorm\norm
\def\norm{\@ifstar{\oldnorm}{\oldnorm*}}
\let\oldsimp\simp
\def\simp{\@ifstar{\oldsimp}{\oldsimp*}}
\makeatother


% declare math operators
\DeclareMathOperator{\lspan}{span} % linear span
\newcommand{\st}[2]{\operatorname{st}_{#1}\left( #2 \right)}
\newcommand{\lk}[2]{\operatorname{lk}_{#1}\left( #2 \right)}

% piecewise linear and other specifics
\newcommand{\pl}{PL }
\newcommand{\pc}{PC }
\newcommand{\cpst}{\operatorname{st}}
\newcommand{\cplk}{\operatorname{lk}}
\newcommand{\triang}{\triangleleft}
\newcommand{\sdivs}{\prec}
\newcommand{\pleq}{\cong}

% Nicer looking other stuffs
\newcommand{\ie}{i.\@ e.\,}

% Nicer looking quantifiers
\newcommand{\fa}[1]{\forall \, {#1} \,:\,}
\newcommand{\ex}[1]{\exists \, {#1} \,:\,}
\newcommand{\exu}[1]{\exists! \, {#1} \,:\,} % Eindeutige Existenz

\newcommand\restr[2]{{% we make the whole thing an ordinary symbol
  \left.\kern-\nulldelimiterspace % automatically resize the bar with \right
  #1 % the function
  \right|_{#2} % this is the delimiter
  }}

% quotients
 \newcommand\quotient[2]{
        \mathchoice
            {% \displaystyle
                \text{\raise1ex\hbox{$#1$}\Big/\lower1ex\hbox{$#2$}}%
            }
            {% \textstyle
                #1\,/\,#2
            }
            {% \scriptstyle
                #1\,/\,#2
            }
            {% \scriptscriptstyle  
                #1\,/\,#2
            }
    }

% equal signs with stuffs on top
\newcommand\eqdef{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily def}}}{=}}}
\newcommand\eqstar{\mathrel{\overset{\makebox[0pt]{\mbox{\normalfont\tiny\sffamily $\circledast$}}}{=}}}

% roman numerals
\renewcommand{\RN}[1]{\uppercase\expandafter{\romannumeral #1\relax}}
\newcommand{\rn}[1]{\lowercase\expandafter{\romannumeral #1\relax}}

\renewcommand{\baselinestretch}{1.2}

% penalties
\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000
