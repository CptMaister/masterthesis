# masterthesis

Public repository of my master thesis, submitted in June 2024 at the University of Augsburg.

All pictures can be used and modified freely. They can be found in the img folder and most are SVG files created in Inkscape and embedded into the tex files as exported PDF files.

The tex-styling is based on slightly modified files of [Milan Zerbin](https://gitlab.com/MilanZerbin)'s [thesis](https://gitlab.com/MilanZerbin/bachelorthesis).

The repo that was used to work on the thesis is private as it includes not publicly available annotated literature.

Check out the websites [winding-numbers.org](http://winding-numbers.org/) and [complex-fibers.org](http://complex-fibers.org/)!
